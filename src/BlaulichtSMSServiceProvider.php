<?php

namespace OVOrg\blaulichtSMS;

use Illuminate\Support\ServiceProvider;

class BlaulichtSMSServiceProvider extends ServiceProvider {
    public function boot() {
        $this->publishes([
            __DIR__."/../config/blaulichtsms.php" => config_path("blaulichtsms.php"),
        ]);
    }

    public function register() {
        $this->app->singleton(Dashboard::class, function () {
            return new Dashboard();
        });

        $this->mergeConfigFrom(
            __DIR__.'/../config/blaulichtsms.php', 'blaulichtsms'
        );
    }
}
