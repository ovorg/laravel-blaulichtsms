<?php

namespace OVOrg\blaulichtSMS;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class Dashboard {
    static private $baseUri = "https://api.blaulichtsms.net";
    static private $sessionId;

    /**
     * Login to the Dasboard API.
     *
     * @return bool If login was successfull
     */
    public static function login(): bool {
        $client = new Client([
            'base_uri' => self::$baseUri,
        ]);

        $json = [
            "customerId" => config('blaulichtsms.customerID'),
            "username"   => config('blaulichtsms.username'),
            "password"   => config('blaulichtsms.password'),
        ];

        try {
            $response = $client->request('POST', '/blaulicht/api/alarm/v1/dashboard/login', [
                'json'    => $json,
                'headers' => [
                    'Content-Type' => "application/json",
                ],
            ]);

            self::$sessionId = json_decode($response->getBody(), true)["sessionId"];
            return true;
        } catch (GuzzleException $e) {
            Log::error($e);
            return false;
        }
    }

    /**
     * Get info from Dashboard API.
     */
    public static function info() {
        // TODO: Cache session id?
        if (!self::$sessionId) {
            self::login(); // TODo: Check for success.
        }

        $client = new Client([
            'base_uri' => self::$baseUri,
        ]);

        try {
            $response = $client->request('GET', '/blaulicht/api/alarm/v1/dashboard/f20ebb17-7461-455b-afee-d73b259c64303tSA2pL8OE');

            return json_decode($response->getBody(), true);
        } catch (GuzzleException $e) {
            return $e;
        }
    }
}
